# Soal-shift-sisop-modul-1-E02-2022
Anggota:
1. Haniif Ahmad Jauhari 5025201224
2. Made Rianja Richo Dainino 5025201236
3. DICKY INDRA KUNCAHYO 5025201250

## Soal 1
Dikerjakan oleh Haniif Ahmad Jauhari 5025201224
<h4>register.sh</h4>
  

       #!/bin/bash
        echo "Input username: "
        read username
        echo "Input password: "
        read -s password
        timestamp=`date +%m/%d/%Y`
        timstamp2=`date +%H:%M:%S`
        if grep -oxq "$username .*" ./users/user.txt
        then
         echo "$timestamp  $timestamp2 REGISTER:ERROR User already exist" >> log.txt
        else
         echo "$username  $password" >> ./users/user.txt
         echo "$timestamp  $timestamp2 REGISTER:INFO User $username registered successfully" >> log.txt
        fi
Penyimpanan data akun pada file ./users/user.txt menggunakan format berikut pada setiap barisnya

> username password

Untuk mengecek apakah username sudah ada, digunakan fungsi grep berikut

    if grep -oxq "$username .*" ./users/user.txt

![](https://lh3.googleusercontent.com/V5ztLkFrcsTNdmAXtl9mLUokw5UfALsYQx-1EUlSQJqWEvTgCoJXUXsrZt9OM6KfsGnIWe2HAd39IzH_t4t-=w1920-h918)

<h4>main.sh</h4>

    #!/bin/bash
    echo "Login"
    echo "Input Username: "
    read username
    echo "Input Password: "
    read -s password

potongan kode tersebut digunakan untuk membaca input username dan password

    if grep -Fxq "$username  $password" ./users/user.txt
Untuk mengecek apakah username dengan password yang terinput berada pada data akun, digunakan fungsi grep diatas

    then
     echo "$timestamp  $timestamp2 LOGIN:INFO User $username logged in" >> log.txt
     echo "Input Command: "
     read command n
Apabila login berhasil, potongan kode diatas digunakan untuk membaca input command dari user

     if [ $command == 'att' ]
     then
         failcount=$(grep -c "LOGIN:ERROR Failed login attempt on user $username" log.txt)
         successcount=$(grep -c "LOGIN:INFO User $username logged in" log.txt)
         total=$(($failcount + $successcount))
         echo "User $username has attempted to login for $total times"
Potongan kode di atas digunakan untuk menjalankan command 'att'. Perhitungan percobaan login dilihat dari file log.txt dan menggunakan command grep untuk menghitung banyaknya.

     elif [ $command == 'dl' ]
     then
         tmp=`date +%Y-%m-%d_`
         echo $tmp
         dir="$tmp$username"
         if zipfile=$(ls | grep -o ".*${username}.zip")
         then
             unzip $zipfile
             dir=${zipfile::-4}
         else
             dir="$tmp$username"
             mkdir $dir
         fi
         count=$(ls $dir |  grep -c "PIC") 
         for ((i=count+1; i<=count+n; i=i+1))
         do
         wget -O ${dir}/PIC_0${i} https://loremflickr.com/320/240
         done
         newdir="$tmp$username"
         zip -r ${newdir}.zip $dir
         rm -r $dir
    fi
Potongan kode diatas digunakan untuk menjalankan command 'dl N'. Untuk mengecek apakah sudah ada file .zip yang bersesuaian dengan suatu user digunakan baris kode berikut

    if zipfile=$(ls | grep -o ".*${username}.zip")
Pada baris kode diatas, digunakan command grep untuk mencari file berakhiran `username.zip` pada list file di direktori program. Jika ketemu, maka zipfile akan memiliki isi dan nilai booleannya akan menjadi true. Jika tidak ketemu, maka zipfile akan kosong dan nilai booleannya akan menjadi false. Setelah itu, file .zip akan di unzip jika ditemukan. Jika tidak ditemukan, maka akan dibuat folder baru.

    count=$(ls $dir |  grep -c "PIC")
Baris kode diatas digunakan untuk menghitung banyaknya file dengan substring "PIC" pada folder yang kita gunakan untuk menyimpan image. Pada baris kode tersebut, digunakan fungsi grep dengan input list file pada direktori folder yang kita gunakan untuk menghitung banyaknya image file "PIC_XX" di folder tersebut.

    for ((i=count+1; i<=count+n; i=i+1))
    do
        wget -O ${dir}/PIC_0${i} https://loremflickr.com/320/240
    done
    newdir="$tmp$username"
    zip -r ${newdir}.zip $dir
    rm -r $dir
Potongan kode diatas digunakan untuk mendownload image sebanyak N kali dimulai dari urutan image yang sudah ada sebelumnya. Kemudian, folder akan di compress menjadi file .zip, lalu folder lama akan dihapus sehingga hanya menyisakan file .zip saja

    else
     echo "$timestamp  $timestamp2 LOGIN:ERROR Failed login attempt on user $username" >> log.txt
    fi
Potongan kode tersebut dijalankan jika user gagal melakukan login.
![](https://lh3.googleusercontent.com/KZpFZHIPWWyfPzUYXmzxOkKdGcSNfWyhuC2pJ9i-83suYUdnVK8kQ0L-gLYe0jYhd_liIhknQZxvrMNwdJZX=w1920-h918)
![](https://lh6.googleusercontent.com/0QzKOJ-_15ZWrIOdQw-7Xb7MkI1LcnzjTWFmxMTHbhiqSXHEB485O-aSN8UCX9RavwV0M2oYpE4gLzrP4CO6=w1920-h918)

## Soal 2
Dikerjakan oleh Made Rianja Richo Dainino 5025201236

    #!/bin/bash
    # Buat folder forensic_log_website_daffainfo_log
    mkdir forensic_log_website_daffainfo_log
    
    # Rata-rata request per jam
    awk -F: 'NR == 1 { min = max = $3 } { if ($3 > max) max = $3; else if ($3 < min) min = $3 } END {print "Rata-rata request per jam adalah", (NR-1)/(max-min+1)}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt
    
    # IP yang paling banyak melakukan request ke server
    awk -F: 'NR>1 { gsub(/"/, "") } { if ( a[$1]++ >= max ) max = a[$1] } END { for (i in a) if(max == a[i]) print "IP yang paling banyak melakukan request ke server adalah:", i, ", sebanyak", a[i]}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
    
    # Banyak request yang menggunakan user-agent curl
    awk -F: '$9 ~ /curl/ {x++} END { print "Banyak request yang menggunakan user-agent curl adalah", x }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
    
    # Daftar IP yang mengakses pada jam 2 pagi tanggal 22
    awk -F: 'NR>1 { gsub(/"/, "") } { if ( $3 ~ /02/ && $2 ~ /^22/ )  a[$1]++ } END { for (i in a) print i }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

<h4>Buat folder forensic_log_website_daffainfo_log</h4>
Untuk membuat folder dengan nama yang ada pada soal kami menggunakan mkdir sebagai berikut

    mkdir forensic_log_website_daffainfo_log
<h4>Rata-rata request per jam</h4>
untuk menghitung rata-rata request per jam, pertama tama kami menyimpan variabel nilai min dan max dengan parameter ke 3 pada file log. selanjutnya pada tiap baris dilakukan pengecekan untuk memastikan nilai yang max menyimpan jam terbesar dan min menyimpan jam terkecil. lalu pada akhir kami melakukan perhitungan rata rata

    awk -F: 'NR == 1 { min = max = $3 } { if ($3 > max) max = $3; else if ($3 < min) min = $3 } END {print "Rata-rata request per jam adalah", (NR-1)/(max-min+1)}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt

<h4>IP yang paling banyak melakukan request ke server</h4>
Untuk menghitung IP yang paling banyak melakukan request ke server, pertama tama dilakukan substitusi untuk menghilangkan tanda petik dua, selanjutnya kami menyimpan semua ip ke dalam array a yang mana pada tiap baris kami lakukan pengecekan untuk menyimpan jumlah request yang paling banyak di antara request ip lain. pada akhir akan dilakukan looping untuk tiap ip dan dicari ip yang memiliki request dengan nilai sama dengan max yang sudah disimpan

    awk -F: 'NR>1 { gsub(/"/, "") } { if ( a[$1]++ >= max ) max = a[$1] } END { for (i in a) if(max == a[i]) print "IP yang paling banyak melakukan request ke server adalah:", i, ", sebanyak", a[i]}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

<h4>Banyak request yang menggunakan user-agent curl</h4>
Untuk mencari banyak request yang menggunakan user-agent crul, kita dapat menggunakannya dengan menghitung pada tiap baris log yang memiliki /curl/ pada parameter ke 9 dan menyimpannya pada variabel x

    awk -F: '$9 ~ /curl/ {x++} END { print "Banyak request yang menggunakan user-agent curl adalah", x }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

<h4>Daftar IP yang mengakses pada jam 2 pagi tanggal 22</h4>
Untuk mencari daftar IP yang mengakses pada jam 2 pagi tanggal 22, pertama tama kami menghapus semua tanda petik dua lalu melakukan pengecekan apabila parameter ke 3 merupakan /02/ dan parameter ke 2 merupakan /^22/ yang mana berarti request terjadi pada jam 2 pagi tanggal 22, apabila benar maka kita simpan ke dalam variabel array a. Pada akhirnya kami mencetak/print semua IP yang disimpan di dalam array

    awk -F: 'NR>1 { gsub(/"/, "") } { if ( $3 ~ /02/ && $2 ~ /^22/ )  a[$1]++ } END { for (i in a) print i }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

![](https://lh3.googleusercontent.com/KllXssVGc30CedWARp33BE7t7w_r05sDNalmxhZeVuaDAZCgpnnHVMqsBuy_-wN_SQwR6FopKgdDLqy6CZfV=w1920-h918-rw)

## Soal 3
Dikerjakan oleh DICKY INDRA KUNCAHYO 5025201250

<h4>minute_log.sh</h4>
Untuk mendapatkan hasil file log tiap menitnya dapat digunakan kode berikut.

    #! /bin/bash
    now=$(date "+%Y%m%d%H%M%S")
    #echo "Filename : /home/kuncahyo/log/metrics_$now.log"
    echo "mem_total,mem_used,mem_free_mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
     mem=$(free -m |awk 'NR==2{print $2","$3","$4","$5","$6","$7","}')
     swap=$(free -m |awk 'NR==3{print $2","$3","$4","}')
     pat=$(du -sh /home/kuncahyo/Sisop/praktikum |awk '{print $2","$1}')
    echo "$mem$swap$pat" >> /home/kuncahyo/log/metrics_$now.log
     sleep 60
    rm /home/kuncahyo/log/metrics_$now.log

Agar script dapat berjalan otomatis setiap menitnya, maka perlu digunakan crontab dengan baris kode berikut

    * * * * * /{minute_log.sh directory}
    
   <h4>aggregate_minutes_to_hourly_log.sh</h4>
   Untuk mendapatkan hasil agregasi file log tiap jamnya, digunakan kode berikut
   

    #! /bin/bash
    now=$(date "+%Y%m%d%H")
    #echo "Filename : /home/kuncahyo/log/metrics_agg_$now.log"
    echo "mem_total,mem_used,mem_free_mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
     mem=$(free -m |awk 'NR==2{print $2","$3","$4","$5","$6","$7","}')
     swap=$(free -m |awk 'NR==3{print $2","$3","$4","}')
     pat=$(du -sh /home/kuncahyo/Sisop/praktikum |awk '{print $2","$1}')
     tot=$mem$swap$pat
     min=
     if [$min -lt  $tot]
     then min=$tot
      
    echo "minimum,$min">> /home/kuncahyo/log/metrics_agg_$now.log
    echo "maximum,$mem$swap$pat"
    echo "average,$mem$swap$pat"
     sleep 1800
    rm /home/kuncahyo/log/metrics_agg_$now.log
Agar script dapat berjalan otomatis setiap jamnya, maka perlu digunakan crontab dengan baris kode berikut

    0 * * * * /{aggregate_minutes_to_hourly_log.sh directory}
    
![](https://lh6.googleusercontent.com/JsKbzqqT2XDMy2V1psDQbOFnhKSkMycICr31pHtR7l7HMFf0MbsEO7b0xnen56oFYxH9TJTRtBoFm11uI4VB=w1920-h918-rw)
