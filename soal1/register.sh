#!/bin/bash

echo "Input username: "
read username
echo "Input password: "
read -s password

timestamp=`date +%m/%d/%Y`
timstamp2=`date +%H:%M:%S`

if grep -oxq "$username .*" ./users/user.txt
then
    echo "$timestamp $timestamp2 REGISTER:ERROR User already exist" >> log.txt
else
    echo "$username $password" >> ./users/user.txt
    echo "$timestamp $timestamp2 REGISTER:INFO User $username registered successfully" >> log.txt
fi
