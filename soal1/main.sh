#!/bin/bash

echo "Login"
echo "Input Username: "
read username
echo "Input Password: "
read -s password

timestamp=`date +%m/%d/%Y`
timstamp2=`date +%H:%M:%S`
if grep -Fxq "$username $password" ./users/user.txt
then
    echo "$timestamp $timestamp2 LOGIN:INFO User $username logged in" >> log.txt
    echo "Input Command: "
    read command n
    if [ $command == 'att' ]
    then
        failcount=$(grep -c "LOGIN:ERROR Failed login attempt on user $username" log.txt)
        successcount=$(grep -c "LOGIN:INFO User $username logged in" log.txt)
        total=$(($failcount + $successcount))
        echo "User $username has attempted to login for $total times"
    elif [ $command == 'dl' ]
    then
	tmp=`date +%Y-%m-%d_`
	echo $tmp
	dir="$tmp$username"
	if zipfile=$(ls | grep -o ".*${username}.zip")
	then
	    unzip $zipfile
	    dir=${zipfile::-4}
	else
	    dir="$tmp$username"
	    mkdir $dir
	fi
	count=$(ls $dir |  grep -c "PIC") 
	for ((i=count+1; i<=count+n; i=i+1))
	do
	wget -O ${dir}/PIC_0${i} https://loremflickr.com/320/240
	done
	newdir="$tmp$username"
	zip -r ${newdir}.zip $dir
	rm -r $dir
    fi
else
    echo "$timestamp $timestamp2 LOGIN:ERROR Failed login attempt on user $username" >> log.txt
fi
