#!/bin/bash

# Buat folder forensic_log_website_daffainfo_log
mkdir forensic_log_website_daffainfo_log

# Rata-rata request per jam
awk -F: 'NR == 1 { min = max = $3 } { if ($3 > max) max = $3; else if ($3 < min) min = $3 } END {print "Rata-rata request per jam adalah", (NR-1)/(max-min+1)}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt

# IP yang paling banyak melakukan request ke server
awk -F: 'NR>1 { gsub(/"/, "") } { if ( a[$1]++ >= max ) max = a[$1] } END { for (i in a) if(max == a[i]) print "IP yang paling banyak melakukan request ke server adalah:", i, ", sebanyak", a[i]}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

# Banyak request yang menggunakan user-agent curl
awk -F: '$9 ~ /curl/ {x++} END { print "Banyak request yang menggunakan user-agent curl adalah", x }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

# Daftar IP yang mengakses pada jam 2 pagi tanggal 22
awk -F: 'NR>1 { gsub(/"/, "") } { if ( $3 ~ /02/ && $2 ~ /^22/ )  a[$1]++ } END { for (i in a) print i }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt